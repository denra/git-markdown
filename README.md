# Цель репозитория
Собрать изученные данные в одном месте

# Начальные знания
1. Создание репозитория  
    `git init`

2. Добавление коммита  
    `git add --all`  
    `git commit`
	
    С его описанием  
    `git commit -m "Your description"`

3. Просмотр истории  
    `git log`
	
# Ветки и слияния

> Зачем нужна ветка?  
> Чтобы выполнить задачу вне основного потока разработки.
	
4. Создание ветки  
    `git branch branch_name`

5. Переключение репозитория в состояние другой ветки  
    `git checkout branch_name`

6. Слияние веток  
    1. *merge* - Объединяет текущую ветку и branch_name  
    `git merge branch_name`

    2. *rebase* - Копирует коммиты из текущей ветки в branch_name  
    `git rebase branch_name`
		
7. Удаление веток  
`git branch -d branch_name`

# Рабочая директория

8. Посмотреть, как git видит папку с кодом.  
`git status`

9. Сказать git, что нужно отслеживать изменения файла new_file (добавить в список индексируемых)  
    `git add new_file`

    Добавление файлов по маске  
    `git add "*.py"`

    Добавление всех файлов  
    `git add --all` или `git add -A`

10. Удаление и перемещение  
    Позволяет полностью отслеживать историю проекта  
    
    `git rm file_name` - удаление  
    `git mv path` - перемещение

11. Отмена add  
    `git reset new_file`

12. [Откаты коммитов](http://ru.stackoverflow.com/questions/431520/%D0%9A%D0%B0%D0%BA-%D0%B2%D0%B5%D1%80%D0%BD%D1%83%D1%82%D1%8C%D1%81%D1%8F-%D0%BE%D1%82%D0%BA%D0%B0%D1%82%D0%B8%D1%82%D1%8C%D1%81%D1%8F-%D0%BA-%D0%B1%D0%BE%D0%BB%D0%B5%D0%B5-%D1%80%D0%B0%D0%BD%D0%BD%D0%B5%D0%BC%D1%83-%D0%BA%D0%BE%D0%BC%D0%BC%D0%B8%D1%82%D1%83 "Откаты коммитов")

13. Игнорировать файл (вообще не видеть его как потенциально интересноый)  
    Добавить в файл *.gitignore*  
    
    Пример файла *.gitignore*:  
    > boring_folder  
    > *.log

# Удалённый репозиторий

За работу с удалённым репозиторием отвечает семейство команд `git remote`

1. 

3. Добавление удалённого репозитория  
    `git remote add remote_repo_alias path_to_remote_repo`  

    *remote_repo_alias* - псевдоним, под которым будет известен  
                        удалённый репозиторий на локальной машине (например, origin)
    
    *path_to_remote_repo* - путь до удалённого репозитория (например, до репозитория на гитхабе)
	
4. Добавить локальные изменения "наружу"  
    `git push remote_repo_alias pushing_branch`

5. Скачать залить к себе "наружный" уровень  
    `git pull remote_repo_alias remote_repo_branch`

# Дополнительно
* [Простое руководство по MarkDown](http://paulradzkov.com/2014/markdown_cheatsheet/ "Руководство по MarkDown")
* [Быстрое введение в Git](https://www.youtube.com/watch?v=mvVsrB46zkc "Введение в Git")